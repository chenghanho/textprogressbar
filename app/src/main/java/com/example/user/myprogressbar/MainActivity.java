package com.example.user.myprogressbar;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    int myProgress = 0;
    TextProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pb = (TextProgressBar) findViewById(R.id.pb);
        pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));
        new Thread(myThread).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Runnable myThread = new Runnable(){
        @Override
        public void run() {
            while (myProgress<100){
                try{
                    System.out.println("SSS");
                    pb.setProgress(myProgress);
                    pb.setText(myProgress+"%");
                    myHandle.sendMessage(myHandle.obtainMessage());
                    Thread.sleep(500);
                }
                catch(Throwable t){
                }
            }
        }

        Handler myHandle = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                myProgress = myProgress + 10;
                pb.setProgress(myProgress);
                pb.setText(myProgress+"%");
            }
        };
    };
}

package com.example.user.myprogressbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ProgressBar;

/**
 * Created by user on 2015/9/30.
 */
public class TextProgressBar extends ProgressBar{
    private String text;
    private Paint textPaint;
    private static int MY_SP_VALUE = 16; //dp

    public TextProgressBar(Context context) {
        super(context);
        initTextProgressBar();
    }

    public TextProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTextProgressBar();
    }

    public TextProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initTextProgressBar();
    }

    private void initTextProgressBar() {
        int pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                MY_SP_VALUE, getResources().getDisplayMetrics());
        text = "0%";
        textPaint = new Paint();
        textPaint.setTextSize(pixel);
        textPaint.setColor(Color.WHITE);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect bounds = new Rect();
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        int x = getWidth() / 2 - bounds.centerX();
        int y = getHeight() / 2 - bounds.centerY();
        canvas.drawText(text, x, y, textPaint);
    }

    public synchronized void setText(String text) {
        this.text = text;
        drawableStateChanged();
    }

    public void setTextColor(int color) {
        textPaint.setColor(color);
        drawableStateChanged();
    }
}
